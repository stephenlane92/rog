<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['genmsg'] = 'Hledání podle všech, či jen vybraných kritérií.';
$string['name'] = 'Jméno';
$string['username'] = 'Uživatelské jméno';
$string['studentid'] = 'ID studenta';
$string['module'] = 'Modul';
$string['academicyear'] = 'Akademický rok';
$string['advanced'] = 'Rozšířené';
$string['students'] = 'Studenti';
$string['graduates'] = 'Absolvent';
$string['leavers'] = 'Odstupující';
$string['leftuniversity'] = 'Opouští školu';
$string['suspended'] = 'Suspendován';
$string['staff'] = 'Zaměstnanec';
$string['staffadmin'] = 'Zaměstnanec (Admin)';
$string['staffsysadmin'] = 'Zaměstnanec (SysAdmin)';
$string['staffstudent'] = 'Zaměstnanec a Student'; 
$string['inactivestaff'] = 'Neaktivní zaměstnanec' ;
$string['externalexaminers'] = 'Externí recenzent';
$string['invigilators'] = 'Dohlížející osoba';
$string['back'] = 'Zpět';
$string['search'] = 'Hledat';
$string['usertasks'] = 'Úkoly uživatele';
$string['viewuserfile'] = 'Zobrazit uživatelské soubory';
$string['createnewuser'] = 'Nový uživatel';
$string['emailuser'] = 'Zaslat uživateli e-mail';
$string['importusers'] = 'Nahrát uživatele';
$string['importmodules'] = 'Nahrát moduly';
$string['anyyear'] = 'libovolný rok';
$string['anymodule'] = '(libovolný modul)';
$string['title'] = 'Titul';
$string['year'] = 'Rok';
$string['course'] = 'Kurz';
$string['na'] = 'N/A'; 
$string['unknown'] = '&lt;neznámý&gt;';
$string['unknowndegree'] = '&lt;neznámý titul&gt';
$string['mr'] = 'Pan';
$string['dr'] = 'MUDr'; 
$string['miss'] = 'Slečna';
$string['mrs'] = 'Paní';
$string['ms'] = 'Paní';
$string['professor'] = 'Profesor';
$string['deleteuser'] = 'Odstranit uživatele';
$string['performsummary'] = 'Přehled výsledků';
$string['clearltilinks'] = 'Clear LTI Links';
?>